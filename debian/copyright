Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gear
Upstream-Contact: Openstack Infrastructure
Source: https://review.openstack.org/p/openstack-infra/gear.git

Files: *
Copyright: (c) 2013-2015, OpenStack Foundation
           (c) 2013-2015, Hewlett-Packard Development Company, L.P.
License: GPL-2-or-Apache-2.0

Files: debian/*
Copyright: (c) 2013, Antoine Musso <amusso@free.fr>
           (c) 2013, Wikimedia Foundation Inc.
           (c) 2015, Thomas Goirand <zigo@debian.org>
License: GPL-2-or-Apache-2.0

License: GPL-2-or-Apache-2.0
 GPL-2 copyright notice:
 .
 This program is free software; you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation; either version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
 St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General Public License v2
 (GPL) can be found in /usr/share/common-licenses/GPL-2.
 .
 Apache-2.0 copyright notice:
 .
 Licensed under the Apache License, Version 2.0 (the "License"); you may
 not use this file except in compliance with the License. You may obtain
 a copy of the License at
 .
      http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 License for the specific language governing permissions and limitations
 under the License.
 .
 On Debian-based systems the full text of the Apache version 2.0 license
 can be found in `/usr/share/common-licenses/Apache-2.0'.
